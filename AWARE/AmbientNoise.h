//
//  AmbientNoise.h
//  AWARE
//
//  Created by Yuuki Nishiyama on 11/26/15.
//  Copyright © 2015 Yuuki NISHIYAMA. All rights reserved.
//

#import "AWARESensor.h"
#import "AWAREKeys.h"
#import <AudioToolbox/AudioToolbox.h>

@interface AmbientNoise : AWARESensor <AWARESensorDelegate>

@end
